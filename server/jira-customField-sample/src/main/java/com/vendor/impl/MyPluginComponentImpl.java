package com.vendor.impl;

import com.atlassian.migration.app.AccessScope;
import com.atlassian.migration.app.AppCloudMigrationGateway;
import com.atlassian.migration.app.ContainerType;
import com.atlassian.migration.app.ContainerV1;
import com.atlassian.migration.app.MigrationDetailsV1;
import com.atlassian.migration.app.PaginatedContainers;
import com.atlassian.migration.app.PaginatedMapping;
import com.atlassian.migration.app.ServerAppCustomField;
import com.vendor.MyPluginComponent;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.migration.app.AccessScope.*;

public class MyPluginComponentImpl implements MyPluginComponent, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(MyPluginComponentImpl.class);
    private final ObjectMapper objectMapper;
    private final AppCloudMigrationGateway gateway;


    public MyPluginComponentImpl(AppCloudMigrationGateway migrationGateway) {
        this.objectMapper = new ObjectMapper();
        this.gateway = migrationGateway;
        this.gateway.registerListener(this);
    }

    /**
     * Just some examples of the operations that can be done as part of an app migration.
     * All of them are optional. You may or may not use them depending on your migration needs
     */
    @Override
    public void onStartAppMigration(String transferId, MigrationDetailsV1 migrationDetails) {

        // Retrieve ID mappings from the migration.
        // A complete guide can be found at https://developer.atlassian.com/platform/app-migration/getting-started/mappings/

        // Fetch containers associated with a migration
        // You can also fetch jira project containers using ContainerType.JiraProject as container type in the call to "getPaginatedContainers"

        // You can also upload one or more files to the cloud. You'll be able to retrieve them through Atlassian Connect
        // Refer file data updload example mentioned in https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/server/basic-sample/
    }

    @Override
    public Map<ServerAppCustomField, String> getSupportedCustomFieldMappings() {
        Map<ServerAppCustomField, String> mappings = new HashMap<>();
        mappings.put(new ServerAppCustomField("Sample Custom Field", "sample-custom-field-type"), "sample-custom-field-cloud");
        return mappings;
    }

    @Override
    public String getCloudAppKey() {
        return "my-cloud-custom-field-app-key";
    }

    @Override
    public String getServerAppKey() {
        return "my-server-custom-field-app-key";
    }

    /**
     * Declare what categories of data your app handles.
     */
    @Override
    public Set<AccessScope> getDataAccessScopes() {
        return Stream.of(APP_DATA_OTHER, PRODUCT_DATA_OTHER, MIGRATION_TRACING_PRODUCT)
                .collect(Collectors.toCollection(HashSet::new));
    }

    @Override
    public void destroy() {
        log.info("Deregistering listener");
        gateway.deregisterListener(this);
    }

    public void onRegistrationAccepted() {
        log.warn("************ Nice! The migration listener is ready to take migrations events");
    }

}
