# Workflow - sample -  A Migration Listener
This is the simplest we can get from a working example for app-migration.

It has no spring-scanner, no buffering against OSGi changes, and minimal dependencies.

## Important pieces to reproduce this

- Import this dependency:
```xml
<dependency>
     <groupId>com.atlassian</groupId>
     <artifactId>atlassian-app-cloud-migration-osgi</artifactId>
     <version>0.4.0</version>
     <scope>provided</scope>
 </dependency>
``` 
- Add the package `com.atlassian.migration.app.*,` in your `<Import-Package>` instruction
- Inject `AppCloudMigrationGateway` in your code
- Register one or more migration listeners

## Building

Go to server app project root folder and then go to server/workflow-sample folder, build the project with:

    mvn clean install

This will produce a P2-plugin jar.

# License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.
