package com.vendor.impl;

import com.atlassian.migration.app.AccessScope;
import com.atlassian.migration.app.AppCloudMigrationGateway;
import com.atlassian.migration.app.MigrationDetailsV1;
import com.vendor.MyPluginComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.migration.app.AccessScope.*;

public class MyPluginComponentImpl implements MyPluginComponent, DisposableBean {

    private static final Logger log = LoggerFactory.getLogger(MyPluginComponentImpl.class);
    private final AppCloudMigrationGateway gateway;

    public MyPluginComponentImpl(AppCloudMigrationGateway migrationGateway) {
        this.gateway = migrationGateway;
        this.gateway.registerListener(this);
    }

    /**
     * Just some examples of the operations that can be done as part of an app migration.
     * All of them are optional. You may or may not use them depending on your migration needs
     */
    @Override
    public void onStartAppMigration(String transferId, MigrationDetailsV1 migrationDetails) {

    }

    @Override
    public Map<String, String> getSupportedWorkflowRuleMappings() {
        Map<String, String> workflowRules = new HashMap<>();
        workflowRules.put("com.vendor.workflow.ParentIssueBlockingCondition", "parentIssueBlockingCondition-cloud");
        workflowRules.put("com.vendor.workflow.CloseParentIssuePostFunction", "closeParentIssuePostFunction-cloud");
        workflowRules.put("com.vendor.workflow.CloseIssueWorkflowValidator", "closeIssueWorkflowValidator-cloud");
        return workflowRules;
    }

    @Override
    public String getCloudAppKey() {
        return "com.vendor.vendor-plugin-workflow-sample";
    }

    @Override
    public String getServerAppKey() {
        return "com.vendor.vendor-plugin-workflow-sample";
    }

    /**
     * Declare what categories of data your app handles.
     */
    @Override
    public Set<AccessScope> getDataAccessScopes() {
        return Stream.of(APP_DATA_OTHER, PRODUCT_DATA_OTHER, MIGRATION_TRACING_PRODUCT)
                .collect(Collectors.toCollection(HashSet::new));
    }

    @Override
    public void destroy() {
        log.info("Deregistering listener");
        gateway.deregisterListener(this);
    }
}
