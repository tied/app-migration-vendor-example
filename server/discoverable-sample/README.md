# Discoverable App -  A Migration Listener

This example uses the `atlassian-app-cloud-migration-listener` library to provide a simple working example using Atlassian Spring Scanner with no OSGi dependencies

## How to use this library

- Import the dependency:
```xml
<dependency>
    <groupId>com.atlassian</groupId>
    <artifactId>atlassian-app-cloud-migration-listener</artifactId>
    <version>1.0.2</version>
</dependency>
``` 

- In `atlassian-plugin.xml` export a component that implements `DiscoverableListener`
```xml
<component key="myPlugin" class="com.vendor.impl.MyPluginComponentImpl" public="true">
    <interface>com.atlassian.migration.app.listener.DiscoverableListener</interface>
</component>
```

## Building

Go to server app project root folder and then go to `server/discoverable-sample` folder, build the project with:

```shell
mvn clean package
```

This will produce a P2-plugin jar which can be installed into a running Confluence or Jira server using (adjust as necessary)
```shell
atlas-install-plugin --http-port 1990 --context-path /confluence
```

# License

Copyright (c) 2021 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.
