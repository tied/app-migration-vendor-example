package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@IgnoreJwt
@RestController
@RequestMapping("webhook")
public class WebhookRegistrationController extends BaseController {

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Void> putWebHook(@RequestBody NotificationEndpoints webhook) {
        return ResponseEntity.ok(call(getFullPath() + "/webhook", HttpMethod.PUT, webhook, Void.class));
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<String> getWebHook() {
        return ResponseEntity.ok(call(this.getFullPath() + "/webhook", HttpMethod.GET, null, String.class));
    }
}
