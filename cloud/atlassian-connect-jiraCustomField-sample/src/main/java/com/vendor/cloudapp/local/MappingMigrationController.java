package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Set;

@IgnoreJwt
@RestController
@RequestMapping("migration")
public class MappingMigrationController extends BaseController {

    @RequestMapping(value = "/mapping/{transferId}/page", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getMappingsForTransferId(@PathVariable String transferId, @RequestParam String namespace, @RequestParam(required = false) String lastEntity, @RequestParam(required = false) String pageSize) {
        return ResponseEntity.ok(call(getFullPath() + "/mapping/" + transferId + "/page?" + paginationQueryParamsNameSpace(lastEntity, pageSize, namespace), HttpMethod.GET, null, String.class));
    }

    @RequestMapping(value = "/mapping/{transferId}/find", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Map> getMappingsForTransferId(@PathVariable String transferId, @RequestParam String namespace, @RequestBody Set<String> ids) {
        return ResponseEntity.ok(call(getFullPath() + "/mapping/" + transferId + "/find?namespace=" + namespace, HttpMethod.POST, ids, Map.class));
    }

    @RequestMapping(value = "/field", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<Void> editCustomFieldValueInIssues(@RequestHeader("Atlassian-Transfer-Id") String transferId, @RequestHeader("Atlassian-Account-Id") String userAaid, @RequestBody JiraIssueFieldValueRequestDto jiraIssueFieldValueRequestDto) {
        return ResponseEntity.ok(callWithCustomHeader(getFullPath() + "/field", HttpMethod.PUT, jiraIssueFieldValueRequestDto, Void.class, transferId, userAaid));
    }

    private String paginationQueryParamsNameSpace(String lastEntity, String pageSize, String namespace) {
        String result = "";
        result += getPaginationQueryParam(lastEntity, pageSize);
        if (!StringUtils.isEmpty(namespace)) result += "&namespace=" + namespace;
        return result.replaceFirst("&", "");
    }

    private String getPaginationQueryParam(String lastEntity, String pageSize) {
        String result = "";
        if (!StringUtils.isEmpty(lastEntity)) result += "&lastEntity=" + lastEntity;
        if (!StringUtils.isEmpty(pageSize)) result += "&pageSize=" + pageSize;
        return result;
    }
}
