package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.vendor.cloudapp.local.BaseController;

@IgnoreJwt
@RestController
@RequestMapping("/rest/api/3/issue/{issueId}")
public class JiraIssueController extends BaseController {
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Void> editIssueFields(@PathVariable String issueId) {
        return ResponseEntity.ok(call(getJiraFullPath() + "/rest/api/3/issue" + issueId,  HttpMethod.PUT, null, Void.class));
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<String> getIssueFields(@PathVariable String issueId) {
        return ResponseEntity.ok(call(getJiraFullPath() + "/rest/api/3/issue/" + issueId , HttpMethod.GET, null, String.class));
    }
}
