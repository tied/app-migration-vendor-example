package com.vendor.cloudapp.connect;

import com.vendor.cloudapp.local.JiraIssueController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.vendor.cloudapp.local.MappingMigrationController;

import org.springframework.beans.factory.annotation.Autowired;

@RestController
@RequestMapping("notification")
public class MigrationNotificationController {
    @Autowired
    private JiraIssueController jiraIssueController;

    @Autowired
    private MappingMigrationController mappingMigrationController;

    private static final Logger logger = LoggerFactory.getLogger(MigrationNotificationController.class);

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> event(@RequestBody MigrationEvent event) {
        logger.info("Received webhook event notification: " + event);

        // When listener-triggered notification is received, get app specific custom fields mappings using namespace jira/classic:appCustomField.
        // Follow step 2 and 3 described in https://developer.atlassian.com/platform/app-migration/migrating-app-custom-fields to add custom field values in migrated project issues.

        if ("app-data-uploaded".equals(event.getEventType())) {
            logger.info("s3Key to get url for uploaded data : " + event.getS3Key());
            logger.info("label of uploaded data: " + event.getLabel());
        }
        return ResponseEntity.ok().build();
    }
}
