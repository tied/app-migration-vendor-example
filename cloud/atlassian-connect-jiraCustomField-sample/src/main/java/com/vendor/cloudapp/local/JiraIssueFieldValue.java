package com.vendor.cloudapp.local;

import com.fasterxml.jackson.annotation.JsonInclude;

public class JiraIssueFieldValue {
    private JiraIssueFieldType _type;

    private int issueID;

    private int fieldID;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String string;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Double number;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String text;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String optionID;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String richText;

    public JiraIssueFieldType get_type() {
        return _type;
    }

    public void set_type(JiraIssueFieldType _type) {
        this._type = _type;
    }

    public int getIssueID() {
        return issueID;
    }

    public void setIssueID(int issueID) {
        this.issueID = issueID;
    }

    public int getFieldID() {
        return fieldID;
    }

    public void setFieldID(int fieldID) {
        this.fieldID = fieldID;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOptionID() {
        return optionID;
    }

    public void setOptionID(String optionID) {
        this.optionID = optionID;
    }

    public String getRichText() {
        return richText;
    }

    public void setRichText(String richText) {
        this.richText = richText;
    }
}
