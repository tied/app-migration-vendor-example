package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.Collections;
import java.util.Optional;

import static com.atlassian.connect.spring.AtlassianHostUser.builder;

public class BaseController {

    public static final String ATLASSIAN_CONNECT_MIGRATION_URL = "/rest/atlassian-connect/1/migration";
    protected static final Logger logger = LoggerFactory.getLogger(BaseController.class);
    @Value("${cloud.site.url}")
    private String cloudSiteUrl;
    @Value("${addon.key}")
    private String addonKey;
    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;
    @Autowired
    private AtlassianHostRepository atlassianHostRepository;

    protected String getFullPath() {
        return cloudSiteUrl + ATLASSIAN_CONNECT_MIGRATION_URL;
    }

    protected String getJiraFullPath() {
        return cloudSiteUrl;
    }

    protected <R, B> R callWithCustomHeader(String url, HttpMethod method, B jsonBody, Class<R> responseType, String transferIdInHeader, String userAaid) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Atlassian-App-Key", addonKey);
        headers.set("Atlassian-Transfer-Id", transferIdInHeader);
        HttpEntity<B> request = new HttpEntity<>(jsonBody, headers);
        return executeRequest(url, method, request, responseType, userAaid);
    }

    protected <R, B> R call(String url, HttpMethod method, B jsonBody, Class<R> responseType) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<B> request = new HttpEntity<>(jsonBody, headers);
        return executeRequest(url, method, request, responseType);
    }

    private <R, B> R executeRequest(String url, HttpMethod method, HttpEntity<B> request, Class<R> responseType) {
        return executeRequest(url, method, request, responseType, null);
    }

    private <R, B> R executeRequest(String url, HttpMethod method, HttpEntity<B> request, Class<R> responseType, String userAaid) {
        R response = null;
        try {
            if (userAaid != null) {
                final Optional<AtlassianHost> firstByBaseUrl = atlassianHostRepository.findFirstByBaseUrl(cloudSiteUrl);
                response = atlassianHostRestClients
                        .authenticatedAs(builder(
                                firstByBaseUrl.orElseGet(() -> {
                                    throw new RuntimeException("Couldn't find Atlassian host for url " + cloudSiteUrl);
                                }))
                                .withUserAccountId(userAaid)
                                .build())
                        .exchange(url, method, request, responseType).getBody();
            } else {
                response = atlassianHostRestClients.authenticatedAsAddon().exchange(url, method, request, responseType).getBody();
            }
        } catch (HttpStatusCodeException e) {
            String errorBody = e.getResponseBodyAsString();
            logger.warn("Error Body:" + errorBody, e);
        }
        return response;
    }
}
