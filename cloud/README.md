App Migration - Cloud App Examples
 ==============
 
 A collection of cloud apps (connect app) that shows how to interact with the App Migration platform. 
 
- atlassian-connect-sample: Example of how to use app migration endpoints and get notifications.
- atlassian-connect-jiraCustomField-sample: Example of how to migrate Jira custom fields using app migration endpoints.
 
 Full documentation of atlassian connect is always available at:

 https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/ 
