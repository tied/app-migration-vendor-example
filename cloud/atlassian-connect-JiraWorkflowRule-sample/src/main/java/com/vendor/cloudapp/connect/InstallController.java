package com.vendor.cloudapp.connect;

import com.atlassian.connect.spring.AddonInstalledEvent;
import com.atlassian.connect.spring.AddonUninstalledEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Installation controller
 */
@Controller
public class InstallController {
    private static final Logger logger = LoggerFactory.getLogger(InstallController.class);

    @EventListener
    public void handleInstall(AddonInstalledEvent event) {
        logger.info("Install event: {}", event);
    }

    @EventListener
    public void handleUninstall(AddonUninstalledEvent event) {
        logger.info("Uninstall event: {}", event);
    }

    @RequestMapping(value = "/enabled", method = RequestMethod.POST)
    public ResponseEntity<Void> handleEnabled(@RequestBody String event) {
        logger.info("Enabled event: {}", event);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/disabled", method = RequestMethod.POST)
    public ResponseEntity<Void> handleDisabled(@RequestBody String event) {
        logger.info("Disabled event: {}", event);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
