package com.vendor.cloudapp.local.workflow;

import java.util.Set;

public class WorkflowsRulesRequest {

    private Set<String> ruleIds;

    private String workflowEntityId;

    public Set<String> getRuleIds() {
        return ruleIds;
    }

    public void setRuleIds(Set<String> ruleIds) {
        this.ruleIds = ruleIds;
    }

    public String getWorkflowEntityId() {
        return workflowEntityId;
    }

    public void setWorkflowEntityId(String workflowEntityId) {
        this.workflowEntityId = workflowEntityId;
    }
}
